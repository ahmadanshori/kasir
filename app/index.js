import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {Provider} from 'react-redux';
import EStyleSheet from 'react-native-extended-stylesheet';

// import store from './config/store';
import Navigator from './config/routes';

// import {createReduxContainer} from 'react-navigation-redux-helpers';

EStyleSheet.build({
  $black: '#000000',
  $white: '#FFFFFF',
  $lightWhite: '#ededed',
  $green: '#03A688',
  $lightGreen: '#04D9B2',
  $darkGreen: '#02735E',
  $red: '#BE1500',
  $darkRed: '#712B22',
  $lightGrey: '#9b9c9e',
  $screenWidth: Dimensions.get('window').width,
  $screenHeight: Dimensions.get('window').height,

  //   $outline: 1,
});

// const AppNavigator = createReduxContainer(Navigator);

// class ReduxNavigator extends Component {
//   backToExit = false;
//   shouldCloseApp = nav => {
//     console.log('nav', nav);
//     return nav.routes[nav.index].index ? false : true;
//   };
//   handleBack = () => {
//     const {dispatch, state} = this.props;
//     if (this.shouldCloseApp(state)) {
//       if (this.backToExit) {
//         return false;
//       }
//       this.backToExit = true;
//       ToastAndroid.show('Tekan sekali lagi untuk keluar', ToastAndroid.SHORT);
//       setTimeout(() => {
//         this.backToExit = false;
//       }, 3000);
//     }
//     dispatch(NavigationActions.back());
//     return true;
//   };
//   componentDidMount() {
//     BackHandler.addEventListener('hardwareBackPress', this.handleBack);
//   }
//   componentWillUnmount() {
//     BackHandler.removeEventListener('hardwareBackPress', this.handleBack);
//   }
//   render() {
//     return (
//       <AppNavigator state={this.props.state} dispatch={this.props.dispatch} />
//     );
//   }
// }

// const mapStateToProps = state => ({
//   state: state.nav,
// });

// const AppWithNavigation = connect(mapStateToProps)(ReduxNavigator);

export default () => {
  // Splash.hide()
  return (
    // <Provider store={store}>
    <Navigator />
    // </Provider>
  );
};

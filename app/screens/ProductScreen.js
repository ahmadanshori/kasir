import React from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

const ProductScreen = () => {
  return (
    <Container>
      <Text>ProductScreen</Text>
    </Container>
  );
};

const styles = EStyleSheet.create({});

export default ProductScreen;

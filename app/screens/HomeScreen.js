import React from 'react';
import {View, Text, FlatList} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';

import {Container} from '../components/Container';
import {ManajerFlatlist} from '../components/Flatlist';

import Data from '../data/ManajemenData';

const HomeScreen = () => {
  const {navigate} = useNavigation();
  const renderItem = ({item}) => {
    return (
      <ManajerFlatlist
        name={item.name}
        des={item.des}
        key={item.id}
        onPress={() => {
          navigate(item.screen);
        }}
      />
    );
  };
  return (
    <Container>
      <FlatList
        data={Data}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.id.toString()}
        renderItem={renderItem}
      />
    </Container>
  );
};

const styles = EStyleSheet.create({});

export default HomeScreen;

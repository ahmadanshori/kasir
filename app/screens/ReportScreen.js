import React from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

const ReportScreen = () => {
  return (
    <Container>
      <Text>ReportScreen</Text>
    </Container>
  );
};

const styles = EStyleSheet.create({});

export default ReportScreen;

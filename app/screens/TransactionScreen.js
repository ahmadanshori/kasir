import React from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import {Container} from '../components/Container';

const TransactionScreen = () => {
  return (
    <Container>
      <Text>TransactionScreen</Text>
    </Container>
  );
};

const styles = EStyleSheet.create({});

export default TransactionScreen;

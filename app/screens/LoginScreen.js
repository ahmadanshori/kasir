import React from 'react';
import {View, Text} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import {useNavigation, useNavigationParam} from 'react-navigation-hooks';

import {Container} from '../components/Container';
import {Button} from '../components/Button';

const LoginScreen = () => {
  const {navigate} = useNavigation();
  return (
    <Container style={styles.container}>
      <Text>Login Bosque</Text>
      <Button
        text="Login"
        onPress={() => {
          navigate('Home');
        }}
      />
    </Container>
  );
};

const styles = EStyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default LoginScreen;

export default [
  {
    id: 1,
    name: 'Barang atau jasa',
    des: 'menambahkan barang atau jasa untuk transaksi',
    screen: 'Product',
  },
  {
    id: 2,
    name: 'Kategori barang',
    des: 'Pengelompokan barang sesuai klasifikasi',
    screen: 'Category',
  },
  {
    id: 3,
    name: 'Manajemen stok',
    des: 'Menambah atau mengurangi stok barang',
    screen: 'StockManagement',
  },
  {
    id: 4,
    name: 'Pembelian Barang',
    des: 'Menambahkan barang dari supplier',
    screen: 'StockOrder',
  },
  {
    id: 5,
    name: 'Pelanggan dan Suplier',
    des: 'Menambahkan data diri customer/pelanggan',
    screen: 'Customer',
  },
  {
    id: 6,
    name: 'Hutang ',
    des: 'Status kredit setelah teransaksi',
    screen: 'Payable',
  },
  {
    id: 7,
    name: 'Piutang',
    des: 'Status piutang setelah teransaksi',
    screen: 'Receivable',
  },
];

import React from 'react';
import {createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
// import {persistStore, persistReducer} from 'redux-persist';
// import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
// import AsyncStorage from '@react-native-community/async-storage';

// import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';

// import rootSaga from '../sagas';
import reducers from '../reducers';

// const persistConfig = {
//   key: 'root',
//     storage: AsyncStorage,
// };
// const persistedReducer = persistReducer(persistConfig, reducers);

const sagaMiddleware = createSagaMiddleware();
// const navMiddleware = createReactNavigationReduxMiddleware(state => state.nav);

let middleware = [];

if (process.env.NODE_ENV === 'development') {
  middleware.push(logger);
}

let store = createStore(reducers, applyMiddleware(...middleware));
// let persistor = persistStore(store);

// sagaMiddleware.run(rootSaga);

export default store;
// export {persistor};

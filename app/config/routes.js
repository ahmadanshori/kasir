import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import EStyleSheet from 'react-native-extended-stylesheet';

import LoginScreen from '../screens/LoginScreen';
import HomeScreen from '../screens/HomeScreen';
import TransactionScreen from '../screens/TransactionScreen';
import ReportScreen from '../screens/ReportScreen';
import ProductScreen from '../screens/ProductScreen';
import CategoryScreen from '../screens/CategoryScreen';
import StockManagementScreen from '../screens/StockManagementScreen';
import StockOrderScreen from '../screens/StockOrderScreen';
import CustomerScreen from '../screens/CustomerScreen';
import PayableScreen from '../screens/PayableScreen';
import ReceivableScreen from '../screens/ReceivableScreen';

const HomeNav = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: ({navigation}) => ({
        title: 'MANAGEMENT',
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Ionicons
              name="md-menu"
              size={25}
              color={EStyleSheet.value('$white')}
              style={styles.icon}
            />
          </TouchableOpacity>
        ),
      }),
    },
    Product: {
      screen: ProductScreen,
      navigationOptions: () => ({
        title: 'Product',
      }),
    },
    Category: {
      screen: CategoryScreen,
      navigationOptions: () => ({
        title: 'Category',
      }),
    },
    StockManagement: {
      screen: StockManagementScreen,
      navigationOptions: () => ({
        title: 'Stock Management',
      }),
    },
    StockOrder: {
      screen: StockOrderScreen,
      navigationOptions: () => ({
        title: 'Stock Order',
      }),
    },
    Customer: {
      screen: CustomerScreen,
      navigationOptions: () => ({
        title: 'Customer',
      }),
    },
    Payable: {
      screen: PayableScreen,
      navigationOptions: () => ({
        title: 'Hutang',
      }),
    },
    Receivable: {
      screen: ReceivableScreen,
      navigationOptions: () => ({
        title: 'Piutang',
      }),
    },
  },
  {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#02735E',
      },
      headerLayoutPreset: 'center',
      headerTintColor: 'white',
    },
  },
);

const TransactionNav = createStackNavigator(
  {
    Transaction: {
      screen: TransactionScreen,
      navigationOptions: ({navigation}) => ({
        title: 'TRANSAKSI',
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.openDrawer()}>
            <Ionicons
              name="md-menu"
              size={25}
              color="#ffffff"
              style={styles.icon}
            />
          </TouchableOpacity>
        ),
        headerStyle: {
          backgroundColor: '#02735E',
        },
        headerLayoutPreset: 'center',
        headerTintColor: 'white',
      }),
    },
  },
  {headerLayoutPreset: 'center'},
);

const ReportNav = createStackNavigator(
  {
    Report: {
      screen: ReportScreen,
      navigationOptions: ({navigation}) => ({
        title: 'LAPORAN',
        headerLeft: (
          <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
            <Ionicons
              name="md-menu"
              size={25}
              color="#ffffff"
              style={styles.icon}
            />
          </TouchableOpacity>
        ),
        headerStyle: {
          backgroundColor: '#02735E',
        },
        headerLayoutPreset: 'center',
        headerTintColor: 'white',
      }),
    },
  },
  {headerLayoutPreset: 'center'},
);

const AppDrawer = createDrawerNavigator(
  {
    HomeDrawer: {
      screen: HomeNav,
      navigationOptions: ({tintColor}) => {
        return {
          title: 'Manajemen',
          drawerIcon: <Ionicons name="ios-home" color={tintColor} size={25} />,
        };
      },
    },
    TransactionDrawer: {
      screen: TransactionNav,
      navigationOptions: ({tintColor}) => {
        return {
          title: 'Transaksi',
          drawerIcon: <AntDesign name="shrink" color={tintColor} size={25} />,
        };
      },
    },
    ReportDrawer: {
      screen: ReportNav,
      navigationOptions: ({tintColor}) => {
        return {
          title: 'Laporan',
          drawerIcon: (
            <Ionicons name="md-list-box" color={tintColor} size={25} />
          ),
        };
      },
    },
  },
  {
    contentOptions: {
      activeTintColor: '#03A688',
      inactiveTintColor: 'gray',
    },
  },
);

const RootNavigations = createSwitchNavigator({
  Login: LoginScreen,
  App: AppDrawer,
});

const styles = EStyleSheet.create({
  icon: {
    marginLeft: 20,
  },
});

export default createAppContainer(RootNavigations);

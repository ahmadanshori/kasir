import React, {Fragment} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const ManajerFlatlist = ({name, des, onPress}) => {
  return (
    <Fragment>
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <View style={styles.iconContainer}>
          <View style={{height: 40, width: 40, backgroundColor: 'red'}} />
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.des}>{des}</Text>
        </View>
      </TouchableOpacity>
      <View style={styles.separator} />
    </Fragment>
  );
};

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  iconContainer: {
    width: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textContainer: {
    width: '80%',
  },
  name: {
    fontSize: 15,
  },
  des: {
    fontSize: 12,
    color: '$lightGrey',
  },
  separator: {
    height: 5,
    backgroundColor: '$lightWhite',
  },
});

export default ManajerFlatlist;

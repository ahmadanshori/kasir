import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {View} from 'react-native';

const ListViewSeparator = () => {
  return <View style={styles.hairline} />;
};

const styles = EStyleSheet.create({
  hairline: {
    height: EStyleSheet.hairlineWidth,
    backgroundColor: '#777',
  },
});

export default ListViewSeparator;

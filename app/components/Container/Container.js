import React from 'react';
import {View, StatusBar} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Container = ({children, backgroundColor, style = {}}) => {
  const containerStyles = [styles.container];
  if (backgroundColor) {
    containerStyles.push({backgroundColor});
  }
  return (
    <View style={[containerStyles, style]}>
      <StatusBar
        backgroundColor={EStyleSheet.value('$darkGreen')}
        barStyle="light-content"
      />
      {children}
    </View>
  );
};

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '$white',
  },
});

export default Container;
